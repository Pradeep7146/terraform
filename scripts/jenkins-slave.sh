#! /bin/bash

# update the system
yum updaye -y

# install java11
amazon-linux-extras install java-openjdk11 -y

# adding the user
sudo useradd devops

# set password for the user

echo "devops" | passwd --stdin devops
echo "ec2-user" | passwd --stdin ec2-user

# modify the sudoers file and add the entry

echo 'devops   ALL=(ALL)  NOPASSWD:ALL' | sudo tee -a /etc/sudoers
echo 'ec2-user   ALL=(ALL)  NOPASSWD:ALL' | sudo tee -a /etc/sudoers

# modify sshd_config file
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
sudo systemctl restart sshd

#install java8
yum install java-1.8.0-openjdk-devel.86_64 -y &>>$LOG
