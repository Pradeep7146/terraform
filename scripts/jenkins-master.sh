#! /bin/bash

hostnamectl set-hostname jenkins-master

# update the system
yum update -y

# install java11
amazon-linux-extras install java-openjdk11 -y


# download and install jenkins

sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat/jenkins.io-2023.key
sudo yum install jenkins git -y

# adding the user
sudo useradd devops

# set password for the user

echo "devops" | passwd --stdin devops
echo "ec2-user" | passwd --stdin ec2-user

# modify the sudoers file and add the entry

echo 'devops   ALL=(ALL)  NOPASSWD:ALL' | sudo tee -a /etc/sudoers
echo 'ec2-user   ALL=(ALL)  NOPASSWD:ALL' | sudo tee -a /etc/sudoers

# modify sshd_config file
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
sudo systemctl restart sshd

# generate ssh key for password less configurations
yes '' | su - devops -c "ssh-keygen -N '' -t RSA -f /home/devops/.ssh/id_rsa" > /dev/null

# start the jenkins
systemctl start jenkins

#enable jenkins
systemctl enable jenkins

# make sure jenkins is on even after reboot
chkconfig jenkins on
