resource "aws_security_group" "web_sec" {
  name = "allow_web"
  description = "this is to allow the web traffic"

  dynamic "ingress" {
    iterator = port
    for_each = var.ports
    content {
      from_port = port.value
      to_port = port.value
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_web_traffic"
  }

}

data "aws_ami" "aws-linux-2-latest" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*"]
  }
}

resource "aws_instance" "jenkins_master" {
  ami = data.aws_ami.aws-linux-2-latest.id
  instance_type = var.instance_type
  key_name = "ansible"
  vpc_security_group_ids = [aws_security_group.web_sec.id]
  associate_public_ip_address = true
  user_data = file("./scripts/jenkins-master.sh")

  tags = {
    Name = "jenkins_master"
  }
}

resource "aws_instance" "jenkins_slave" {
  ami = data.aws_ami.aws-linux-2-latest.id
  instance_type = var.instance_type
  key_name = "ansible"
  vpc_security_group_ids = [aws_security_group.web_sec.id]
  associate_public_ip_address = true
  user_data = file("./scripts/jenkins-slave.sh")

  tags = {
    Name = "jenkins_slave"
  }
}
